#include<bits/stdc++.h>
using namespace std;

class Stack {
 int *arr;
 int top, MaxLimit;
 
public:
 Stack(int size){
   
   MaxLimit = size;
   arr = new int[MaxLimit];
   top = -1;
 }
 
 bool IsEmpty() {
     return (top < 0);
 }

 void push(int item){
     if(top < MaxLimit - 1) 
     arr[++top] = item;
 }
 
 int pop(){
    if(top == -1)
    return 0;
    return arr[top--];
 }
 
 int peek() {
    if(top == -1)
       return 0;
       return arr[top];
 }
 
 void clear() {
       top = -1;
 }
 
 int size() {
     return top;
 }
 
};

int GetBalancedSubArrayCount(int size) {
	int value = 0, maxCount = 0, count = 0, tempCount = 0;
	bool flag = false;
	Stack stack(size);
	for(int i = 0; i < size; i++) {
		cin>>value;
		
		if(value > 0) {
		    stack.push(value);
		    if(stack.size() >= tempCount) {
		        tempCount = stack.size();
		        flag = true;
		    } else {
		        flag = false;
		    }
		} else {
		    int item = -1 * stack.peek();
		    
		    if(value == item && flag) {
		        count += 2;
		        if(stack.size() == 0) {
		            maxCount += count;
		            stack.clear();
		            count = 0;
		        }
		        stack.pop();
		    } else {
		        if(maxCount < count) {
		            maxCount = count;
		        }
		        count = 0;
		        stack.clear();
		    }
		}
	}
	
	if(maxCount < count) {
	    maxCount = count;
	}
	return maxCount;
}

int main() {
	int limit;
	int MAX_LIMIT = 200000;
	cin>>limit;
	
	if(limit >= 1 && limit <= MAX_LIMIT) {
		cout<<GetBalancedSubArrayCount(limit);
	}
	return 0;
}